import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AuthState } from '../state/auth.state';
import { LogOut } from '../state/auth.actions';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Select(AuthState.getUser) userState: Observable<string>;
  public token: String;
  public logIn: boolean;

  constructor(
    private _userService: UserService,
    private _router: Router,
    private store: Store
  ) {
    this.logIn = false;
    this.checkLogin();
  }
  
  checkLogin() {
    const tokenStorage = JSON.parse(localStorage.getItem('@@STATE'));
    if(tokenStorage !== null && tokenStorage.auth.token !== null) {
      this.logIn = true;
    } else {
      this.logIn = false;
    }
  }

  logOut() {
    this.store.dispatch(new LogOut());
    this._router.navigate(['/']);
  }
  
  ngOnInit() {
  }

}
