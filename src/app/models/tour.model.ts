export interface Tour {
    location: String
    city: String
    marker_long: number
    marker_lat: number
    how_to_find_me: String
    title: String
    slug: String
    detail: String
    images: any
    list_times: any
}