import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { TourComponent } from './admin/tour/tour.component';
import { ListTourComponent } from './list-tour/list-tour.component';
import { ShowTourComponent } from './show-tour/show-tour.component';

const routes: Routes = [
  {
    path: '', component: ListTourComponent
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'admin/dashboard', component: DashboardComponent
  },
  {
    path: 'admin/tour', component: TourComponent
  },
  {
    path: 'tours', component: ListTourComponent
  },
  {
    path: 'tour/:slug', component: ShowTourComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
