import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TourService } from '../services/tour.service';
import { TimeService } from '../services/time.service';
import * as moment from 'moment';


@Component({
  selector: 'app-show-tour',
  templateUrl: './show-tour.component.html',
  styleUrls: ['./show-tour.component.scss']
})
export class ShowTourComponent implements OnInit {

  slug: String;
  tour: any;
  today: Date = new Date();
  selectedDate: Date;
  times: any[];
  selectDay: any;
  showTimes: boolean;
  noResults: boolean;

  constructor(
    private _route: ActivatedRoute,
    private _tourService: TourService,
    private _timeService: TimeService
    //private _renderer: Renderer2,
  ) {
    this.tour = {};
    this.showTimes = false;
    this.noResults = false
  }

  ngOnInit() {
    this._route.params.subscribe(params => {
      this.slug = params['slug'];
      this.showTour();
    });
    //console.log(moment().format('dddd'));
  }

  // ngAfterViewInit() {
  //   let buttons = document.querySelectorAll('mat-calendar mat-calendar-header button');

  //   if (buttons) {
  //     Array.from(buttons).forEach(button => {
  //       this._renderer.listen(button, "click", () => {
  //         console.log(moment().format('MMMM'));
  //       });
  //     })
  //   }
  // }

  showTour() {
    this._tourService.showTour(this.slug).subscribe( response => {
      console.log(response);
      if(response['status'] === 'success') {
        this.tour = response['tour'];
      }
    }, err => {
      console.log(err);
    });
  }

  onDaySeleted(date) {
    // console.log(this.selectedDate);
    // console.log(`Selected: ${moment(this.selectedDate).format('dddd')}`);

    this.selectDay = moment(this.selectedDate).format('dddd, D MMMM');

    const data = {
      tour_id: this.tour.id,
      day: moment(this.selectedDate).format('dddd')
    }

    this._timeService.searchDay( data ).subscribe( response => {
      //console.log(response['status']);
      if(response['status'] === 'success') {
        //console.log(response);
        this.times = response['time'];
        this.showTimes = response['time'].length <= 0 ? false : true;
        this.noResults = response['time'].length <= 0 ? true : false;
      }else{
        console.log('error');
        this.showTimes = false;
      }
    }, err => {
      console.log(err);
      this.showTimes = false;
    });
  }

}
