import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CityService } from '../services/city.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() type: String;
  public search: any;
  public cities: any;

  constructor(
    private _cityService: CityService
  ) {
  }

  ngOnInit() {
    console.log("Input: ", this.type);
    this.listCities();
  }

  listCities() {
    this._cityService.listCities().subscribe( response => {
      console.log(response);
      if(response['status'] === 'success') {
        this.cities =  response['cities'];
        console.log(this.cities)
      }
    }, err => {
      console.log(err);
    });
  }

  searchTour(form:NgForm) {

  }

  showCities() {
    console.log("eeoo");
  }

}
