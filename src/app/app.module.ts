import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';

//States
import { AuthState } from './state/auth.state';

//Libaries
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxDropzoneModule } from 'ngx-dropzone';
import {
  MatInputModule,
  MatFormFieldModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatButtonToggleModule,
  MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';

//Services
import { UserService } from './services/user.service';
import { TourService } from './services/tour.service';
import { CityService } from './services/city.service';
import { TimeService } from './services/time.service';

//Components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { TourComponent } from './admin/tour/tour.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ShowTourComponent } from './show-tour/show-tour.component';
import { ListTourComponent } from './list-tour/list-tour.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    DashboardComponent,
    TourComponent,
    ListTourComponent,
    FooterComponent,
    HeaderComponent,
    ShowTourComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxsModule.forRoot([AuthState], {developmentMode: true}),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDZvqZZquExApyFlLU0LUnmwDl-0okDs9g',
      libraries: ["places"],
      language: 'es',
    }),
    MatGoogleMapsAutocompleteModule.forRoot(),
    BrowserAnimationsModule,
    NgxDropzoneModule,
    MatInputModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [
    UserService,
    TourService,
    CityService,
    TimeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
