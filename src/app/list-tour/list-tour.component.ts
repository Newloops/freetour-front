import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { TourService } from '../services/tour.service';
import { Tour } from "../models/tour.model";
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-list-tour',
  templateUrl: './list-tour.component.html',
  styleUrls: ['./list-tour.component.scss']
})
export class ListTourComponent implements OnInit {

  public tours: Tour[];
  public urlImages: string;

  constructor(
    private titleService: Title,
    private _tourService: TourService
  ) {
    this.titleService.setTitle('FreeTour | Lista de tours');
    this.urlImages = environment.urlImage;
  }

  listTours() {
    this._tourService.listTours().subscribe( response => {
      //console.log(response);
      if(response['status'] === 'success') {
        console.log("success");
        this.tours = response['tours'];
        console.log(this.tours);
      }else{
        console.log('error');
      }
    }, err => {
      console.log(err);
    });
  }

  ngOnInit() {
    this.listTours();
  }

}
