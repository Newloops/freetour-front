import { State, Store, Action, StateContext, Selector } from '@ngxs/store';
//import { Auth } from '../models/auth.model';
import { LogIn, LogOut } from './auth.actions';

export class AuthStateModel {
    token: string
}

@State<AuthStateModel>({
    name: 'auth',
    defaults: {
        token: null
    }
})

export class AuthState {

    @Selector()
    static getUser(state: AuthStateModel) {
        return state.token
    }

    @Action(LogIn)
    logIn({ getState, patchState }: StateContext<AuthStateModel>, { payload }: LogIn) {
        const state = getState();
        patchState({
            token: payload.token
        })
    }

    @Action(LogOut)
    logOut({ getState, patchState }: StateContext<AuthStateModel>) {
        const state = getState();
        patchState({
            token: null
        })
    }

    // @Action(RemoveUser)
    // remove({ getState, patchState }: StateContext<UserStateModel>, { payload }: RemoveUser) {
    //     patchState({
    //         user: getState().user.filter(a => a.token != payload)
    //     })
    // }

}