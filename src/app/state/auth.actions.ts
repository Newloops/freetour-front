import { Auth } from '../models/auth.model';

export class LogIn {
    static readonly type = '[Auth] Log In';
    //constructor(public payload: String) {}
    constructor(public payload: Auth) {}
}

export class LogOut {
    static readonly type = '[Auth] Log Out';
    //constructor(public payload: string) {}
}