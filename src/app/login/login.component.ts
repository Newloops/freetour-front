import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { LogIn } from '../state/auth.actions';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: any;
  public showAlertError: boolean;
  public loading: boolean;

  //user: Observable<String>;

  constructor(
    private _userService: UserService,
    private _router: Router,
    private store: Store,
  ) {
    this.loginForm = {
      email: '',
      password: ''
    };
    this.showAlertError = false;
    this.loading = false;

    //this.user = this.store.select(state => state.user.token);
  }

  login( form:NgForm ) {
    this.loading = true;
    if(form.status=='VALID'){
      this._userService.getLogin(this.loginForm).subscribe( response => {
        if(response['status'] === 'success') {
          this.showAlertError = false;
          this.loading = false;
          this.store.dispatch(new LogIn({token: response['token']}));
          this._router.navigate(['admin/tour']);
        }else{
          this.showAlertError = true;
          this.loading = false;
        }
      }, err => {
        console.log(err);
      });
    }else{
      this.showAlertError = false;
      this.loading = false;
    }
  }

  ngOnInit() {
    // this.email = new FormControl('', [
    //   Validators.required,
    //   Validators.email
    // ]);
    // this.password = new FormControl('', [
    //   Validators.required,
    //   Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z])([a-zA-Z0-9]+)$')
    // ]);
  }

}
