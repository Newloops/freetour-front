import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AuthState } from '../../state/auth.state';
import { LogOut } from '../../state/auth.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  //user$: Observable<String>;
  @Select(AuthState.getUser) userState: Observable<string>;
  public token: String;

  constructor(
    private _userService: UserService,
    private _router: Router,
    private store: Store,
  ) {
    //this.user$ = this.store.select(state => state.user.token);
  }

  ngOnInit() {
  }

}
