import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Location, Appearance } from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import { Tour } from "../../models/tour.model";
import { NgForm } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { TourService } from '../../services/tour.service';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AuthState } from '../../state/auth.state';
import { LogOut } from '../../state/auth.actions';

@Component({
  selector: 'app-tour',
  templateUrl: './tour.component.html',
  styleUrls: ['./tour.component.scss']
})
export class TourComponent implements OnInit {

  @Select(AuthState.getUser) userState: Observable<string>;
  public token: String;

  public appearance = Appearance;
  public zoom: number;
  public latitude: number;
  public longitude: number;
  public selectedAddress: PlaceResult;
  public addressLabelText: String;
  public placeholderText: String;
  public requiredErrorText: String;
  public invalidErrorText: String;
  public tour: Tour;
  public showAlert: boolean;
  public loading: boolean;
  public images: any;
  public showBtnResetImages: boolean;
  public days: any[];
  public hours: any[];
  public times: any[];
  public showButtonToggle: boolean;
 
  constructor(
    private titleService: Title,
    private _router: Router,
    private store: Store,
    private _userService: UserService,
    private _tourService: TourService,
  ) {
    this.showButtonToggle = true;
    this.checkToken();
    this.hours = [{
      value: '10:30'
    }];
    this.times = []
    this.createTimes();
  }

  checkToken() {
    const tokenStorage = JSON.parse(localStorage.getItem('@@STATE'));
    if(tokenStorage !== null && tokenStorage.auth.token !== null) {
      this.userState.subscribe(res => this.token = res);
      this._userService.checkToken(this.token).subscribe( response => {
        if(response['status'] === 'error') {
          this.store.dispatch(new LogOut());
          this._router.navigate(['/']);
        }
      }, err => {
        console.log(err);
      });
    } else {
      this.showButtonToggle = false;
      this._router.navigate(['/login']);
    }
  }
 
  ngOnInit() {

    this.titleService.setTitle('FreeTour | Crear un tour');
 
    this.zoom = 16;
    this.latitude = 52.520008;
    this.longitude = 13.404954;
    this.addressLabelText = 'Lugar donde se realiza el tour'
    this.placeholderText = 'Escribe aquí un lugar';
    this.requiredErrorText = 'Campo obligatorio';
    this.invalidErrorText = 'Campo obligatorio';
 
    this.setCurrentPosition();

    this.tour = {
      location: '',
      city: '',
      marker_long: null,
      marker_lat: null,
      how_to_find_me: '',
      title: '',
      slug:'',
      detail: '',
      images: [],
      list_times: []
    }

    this.showAlert = false;
    this.loading = false;
    this.images = [];
    this.showBtnResetImages = false;
    
  }
 
  setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        console.log("Position: ", position);
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 16;
        this.tour.marker_lat = position.coords.latitude;
        this.tour.marker_long = position.coords.longitude;
      });
    }
  }
 
  onAutocompleteSelected(result: PlaceResult) {
    console.log('onAutocompleteSelected: ', result);
    this.tour.location = result.vicinity;
    result.address_components.forEach((value, index) => {
      if(value.types[0] === 'administrative_area_level_2') {
        this.tour.city = value.long_name;
      }
    });
  }
 
  onLocationSelected(location: Location) {
    console.log('onLocationSelected: ', location);
    this.latitude = location.latitude;
    this.longitude = location.longitude;
    this.tour.marker_lat = location.latitude;
    this.tour.marker_long = location.longitude;
  }

  markerDragEnd(location: any) {
    console.log('markerDragEnd: ', location.coords);
    this.latitude = location.coords.lat;
    this.longitude = location.coords.lng;
    this.tour.marker_lat = location.coords.lat;
    this.tour.marker_long = location.coords.lng;
  }

  setTour(form:NgForm) {
    this.loading = true;
    console.log(form.status);
    if(form.status=='VALID'){
      this.tour.images = this.images;
      this.tour.list_times.push({
        days: this.days,
        hours: this.hours
      });
      console.log(this.tour);
      this._tourService.createTour( this.tour ).subscribe( response => {
        console.log(response['status']);
        if(response['status'] === 'success') {
          console.log("creado");
          this.showAlert = true;
          this.loading = false;
        }else{
          console.log('error');
          this.showAlert = false;
          this.loading = false;
        }
      }, err => {
        console.log(err);
      });
    }else{
      this.loading = false;
    }
  }

  onFilesAdded(files: File[]) {

    console.log(files);
    this.images = [];
    this.checkIamges();

    files.forEach(file => {
      const reader = new FileReader();
   
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
   
        // this content string could be used directly as an image source
        // or be uploaded to a webserver via HTTP request.
        //console.log(content);
        this.images.push(content);
      };
   
      // use this for basic text files like .txt or .csv
      //reader.readAsText(file);
   
      // use this for images
      reader.readAsDataURL(file);
    });

    console.log(this.images);

  }

  checkIamges() {
    if(this.images.length > 0) {
      this.showBtnResetImages = false;
    }else{
      this.showBtnResetImages = true;
    }
  }

  addHour() {
    this.hours.push({
      value: '10:30'
    });
  }

  createTimes() {
    var x = 15; //minutes interval
    var tt = 0; // start time
    //loop to increment the time and push results in array
    for (var i=0; tt<24*60; i++) {
      var hh = Math.floor(tt/60); // getting hours of day in 0-24 format
      var mm = (tt%60); // getting minutes of the hour in 0-55 format
      this.times.push({
        value: ("0" + (hh % 24)).slice(-2) + ':' + ("0" + mm).slice(-2),
        name:  ("0" + (hh % 24)).slice(-2) + ':' + ("0" + mm).slice(-2) + 'h'
      });
      tt = tt + x;
    }
  }

}
