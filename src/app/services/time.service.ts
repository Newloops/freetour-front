import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TimeService {

  constructor(public _http: HttpClient) {

  }

  searchDay(params) {

    let urlConnect = environment.urlApi + 'time/search';

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._http.get(urlConnect, {headers: headers, params: params});

  }
}
