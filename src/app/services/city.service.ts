import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(public _http: HttpClient) {

  }

  listCities() {

    let urlConnect = environment.urlApi + 'cities';

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._http.get(urlConnect, {headers: headers});

  }

}
