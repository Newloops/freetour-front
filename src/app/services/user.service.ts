import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class UserService {

    constructor(public _http: HttpClient) { }

    checkToken(token) {

        let urlConnect = environment.urlApi + 'check-token';

        let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                       .set('Authorization', 'Bearer ' + token);

        return this._http.post(urlConnect, null, {headers: headers});

    }

    getLogin(params) {

        let urlConnect = environment.urlApi + 'login';

        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.post(urlConnect, params, {headers: headers});

    }

    getUser() {

        let urlConnect = environment.urlApi + 'user';

        let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                       .set('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9mcmVldG91ci1hcGkudGVzdFwvYXBpXC9sb2dpbiIsImlhdCI6MTU2MDk2NjQwMywiZXhwIjoxNTYwOTcwMDAzLCJuYmYiOjE1NjA5NjY0MDMsImp0aSI6IkY5MTlXVW9DM1R5UkpKZEwiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.Ooxr2x5xUNOcVHGdVpq5ybGeG7FU9JfRp6nfqu6bVrw');

        return this._http.post(urlConnect, null, {headers: headers});

    }

    getOpen() {

        let urlConnect = environment.urlApi + 'open';

        return this._http.get(urlConnect);

    }

}