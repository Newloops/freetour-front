import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable()
export class TourService {
  private tokenStorage: any;

  constructor(public _http: HttpClient) {
    this.tokenStorage = JSON.parse(localStorage.getItem("@@STATE"));
  }

  createTour(params) {
    let urlConnect = environment.urlApi + "tours/create";

    let headers = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + this.tokenStorage.auth.token);

    return this._http.post(urlConnect, params, { headers: headers });
  }

  listTours() {
    let urlConnect = environment.urlApi + "tours";

    return this._http.get(urlConnect);
	}
	
	showTour($slug) {
    let urlConnect = environment.urlApi + "tours/" + $slug;

		return this._http.get(urlConnect);
  }
}
