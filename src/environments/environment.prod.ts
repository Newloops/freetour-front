export const environment = {
  production: true,
  urlApi: 'http://freetour-api.developcode.es/api/',
  urlImage: 'http://freetour-api.developcode.es/images_tour/'
};
